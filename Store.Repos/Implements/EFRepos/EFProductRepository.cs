﻿using Store.Data.Entities;
using Store.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Repos.Implements.EFRepos
{
    public class EFProductRepository : EFBaseRepository<Product>,IProductRepository
    {
        public EFProductRepository(DataContext context) : base(context)
        {
        }
    }
}
