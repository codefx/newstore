﻿using Store.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Repos.Interfaces
{
    public  interface IProductRepository :IRepository<Product>
    {

    }
}
