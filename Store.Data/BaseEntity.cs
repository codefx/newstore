﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Data
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
    }
}
